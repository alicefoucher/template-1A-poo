from operations.transformers.selectionvariable import SelectionVariableTransformer
from operations.importer.JSONimporter import JSONimporter
from table import Table
from operations.estimators.estimators import Estimators
from operations.transformers.transformers import Transformers
from operations.transformers.changementspatial import ChangementSpatialTransformer
from outils.indicevariable import IndiceVariable
from operations.transformers.fenetrage import FenetrageTransformer
from operations.transformers.jointure import JointureTransformer
from operations.transformers.normalisation import NormalisationTransformer
from operations.transformers.selectiondonnees import SelectionDonneesTransformer
from operations.transformers.moyenne_glissante import MoyenneGlissanteTansformer
from operations.estimators.moyenne import MoyenneEstimator
from operations.estimators.variance_ecart_type import Variance_ecart_typeEstimator
from operations.importer.CSVimporter import CSVimporter
from sorties.sorties import Sorties
from sorties.nbtotal import NbTotal
import time 
import copy
from datetime import datetime
from datetime import timedelta
from operations.transformers.moyenne_glissante import MoyenneGlissanteTansformer
from operations.estimators.kmeans import KmeansEstimator
from sorties.evolmoyparsem import EvolutionMoyenneParSemaine2
from sorties.nbentre2datespardep import NbEntreDeuxDatesParDep
from sorties.evolmoy import EvolutionMoyenne
from sorties.vacances import vacances
from pipeline import Pipeline
from graphique import Graphique


csv_nouveaux_covid=CSVimporter(True, folder='./data/', filename='donnees-hospitalieres-nouveaux-covid19-2021-05-11-19h05.csv')
table_nouveaux_covid= csv_nouveaux_covid.execute()

table4 = copy.copy(table_nouveaux_covid)
pile4 = Pipeline()
pile4.add_step(EvolutionMoyenneParSemaine2("2020-12-04", "2021-05-04", ["incid_hosp"]))
resultat4=pile4.run(table4)
Graphique("incid_hosp","dates","graphique").execute(resultat4)
    #a=EvolutionMoyenneParSemaine2("2021-02-03", "2021-03-03", ["incid_hosp"]).execute(table_nouveaux_covid)
    #print(a.rows)
    #print(str((datetime.strftime(datetime.strptime("2021-03-03", "%Y-%m-%d") + timedelta(15),"%Y-%m-%d"))))