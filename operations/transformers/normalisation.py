#FONCTIONNE !

from table import Table 
from operations.transformers.transformers import Transformers
from outils.indicevariable import IndiceVariable
from operations.estimators.moyenne import MoyenneEstimator
from operations.estimators.variance_ecart_type import Variance_ecart_typeEstimator

class NormalisationTransformer(Transformers):
    """Classe qui normalise les données d'une variable dans une table donnée. Si la variance de cette variable est nulle, il y aura un message d'erreur.

    Attribut
    ----------
    variable : str
        variable sur laquelle on va normaliser

    Méthode
    ----------
    execute() 
        normalise les données de la variable dans la table
    
    Exemple
    --------
    >>> data = Table(["Nom", "Age", "couleur_yeux", "date_naissance"], [["Berova", 20,"Vert", "2001-05-12"],["Foucher", 21,"Marron", "2000-03-08"],
    ["Kiren", 21, "Marron", "1999-09-06"],["Lucas",20,"Bleu", "2000-06-23"],["Rompisch",0.612,"Vert", "2020-09-20"],["Titouan",20,"Bleu", "2000-09-01"]])
    >>> new_data = NormalisationTransformer("Age").execute(data)
    >>> print(new_data.rows)
    [['Berova', 0.053092461859716444, 'Vert', '2001-05-12'], ['Foucher', 0.07141284207355925, 'Marron', '2000-03-08'], 
    ['Kiren', 0.07141284207355925, 'Marron', '1999-09-06'], ['Lucas', 0.053092461859716444, 'Bleu', '2000-06-23'], 
    ['Rompisch', -0.3021030697262679, 'Vert', '2020-09-20'], ['Titouan', 0.053092461859716444, 'Bleu', '2000-09-01']]
    """

    def __init__(self, variable) :
        """
        Attribut
        ----------
        variable : str
            variable sur laquelle on va normaliser

        """
        super().__init__()
        self.variable = variable

    def execute(self, table):
        """Normalise les données de la variable dans la table

        Attribut
        ----------
        table : Table

        Retours
        -------
        Table
            table dont la variable a été normalisée
        str
            message d'erreur si la variance est nulle

        """
        header = table.header
        rows = table.rows
        indvar=IndiceVariable(self.variable)
        indice_var = indvar.execute(table)
        if indice_var == -1 :
            return("table inchangée")
        else:
            moyenne = MoyenneEstimator(self.variable).execute(table)
            variance = Variance_ecart_typeEstimator(self.variable).variance(table)
            if variance == 0:
                return("opération impossible")
            else:
                for row in rows:
                    row[indice_var] = (float(row[indice_var])-moyenne)/variance
                return(Table(header,rows))

    if __name__ == '__main__':
        import doctest
        doctest.testmod() 
