# FONCTIONNE !
from table import Table
from operations.transformers.transformers import Transformers
from outils.indicevariable import IndiceVariable

class SelectionVariableTransformer(Transformers):
    """Classe qui sélectionne les données de la table des variables choisies.
    
    Attribut
    ----------
    variables : list[str]
        variables gardées

    Méthode
    ----------
    execute() 
        sélectionne les données des variables définies

    Exemple
    ----------
    >>> data = Table(["Nom", "Age", "couleur_yeux", "date_naissance"], [["Berova", 20,"Vert", "2001-05-12"],["Foucher", 21,"Marron", "2000-03-08"],
    ["Kiren", 21, "Marron", "1999-09-06"],["Lucas",20,"Bleu", "2000-06-23"],["Rompisch",0.612,"Vert", "2020-09-20"],["Titouan",20,"Bleu", "2000-09-01"]])
    >>> new_data = SelectionVariableTransformer(["Nom", "Age"]).execute(data)
    >>> print(new_data.rows)
    [['Berova', 20], ['Foucher', 21], ['Kiren', 21], ['Lucas', 20], ['Rompisch', 0.612], ['Titouan', 20]]
    >>> print(new_data.header)
    ['Nom', 'Age']
    """

    def __init__(self, variables):
        """
        Attributs
        ----------
        variables : list[str]
            variables qu'on va garder
        """
        super().__init__()
        self.variables = variables
        
    def execute(self, table):
        """Sélectionne les données des variables définies

        Attribut
        ----------
        table : Table

        Retour
        -------
        Table
            table avec les données des variables souhaitées
        
        
        """
        header = table.header
        rows = table.rows

        indice_vars = [IndiceVariable(variable).execute(table) for variable in self.variables]
        header_final = []
        rows_finales=[]

        for indice_var in indice_vars:
            header_final.append(header[indice_var])
            
        for row in rows :
            row_finale=[row[i] for i in indice_vars]
            rows_finales.append(row_finale)

        return(Table(header_final,rows_finales))
    
    if __name__ == '__main__':
        import doctest
        doctest.testmod()


