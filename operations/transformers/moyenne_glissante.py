#FONCTIONNE !
import copy
from table import Table
from operations.transformers.transformers import Transformers
from outils.indicevariable import IndiceVariable

class MoyenneGlissanteTansformer(Transformers):
    """
    Attributs
    ----------
    variables : list[str]
        sur quelles variables allons-nous lisser nos observations
    nb_obs : int
        sur combien d'observations lissons-nous

    Méthode
    ----------
    execute() 
        applique le lissage selon la variable référencée

    Exemple
    ----------
    >>> data = Table(["Nom", "Age", "couleur_yeux","poids"], [["Berova", 20,"Vert","12"],["Foucher", 21,"Marron","70"],["Kiren", 21, "Marron","2351"],
    ["Lucas",20,"Bleu","65"],["Rompisch",0.612,"Vert","3"],["Titouan",20,"Bleu","70"]])
    >>> datalisse = MoyenneGlissanteTransformer("age",2).execute(data)
    >>> print(datalisse.header)
    ['Nom', 'Age', 'couleur_yeux', 'poids']
    >>> print(datalisse.rows)
    [['Berova', 20, 'Vert', 'NEANT'], ['Foucher', 21, 'Marron', 236.25], ['Kiren', 21, 'Marron', 7934.625], ['Lucas', 20, 'Bleu', 219.375], ['Rompisch', 0.612, 'Vert', 10.125], ['Titouan', 20, 'Bleu', 'NEANT']]
    """

    def __init__(self, variables, nb_obs):
        """
        Attributs
        ----------
        variables : list[str]
            sur quelles variables allons-nous lisser nos observations
        nb_obs : int
            sur combien d'observations lissons-nous
        """
        super().__init__()
        self.variables = variables
        self.nb_obs = nb_obs #nombre d'observations prises en compte

    def execute(self,table):
        """
        Attribut
        ----------
        table : Table

        Retour
        -------
        Table
            table lissée

        """
        rows=table.rows
        indice_vars=[IndiceVariable(variable).execute(table) for variable in self.variables]
        table_lissee=copy.copy(table)
        rows_lissees=table_lissee.rows
        k=int(float(self.nb_obs//2)) #pour une meilleure lecture du code
        for row in range(len(rows)-2*k) :
            somme=[0 for variable in self.variables]
            for j in range (len(self.variables)):
                for i in range(k) :
                    somme[j] +=float(rows[row+k-i][indice_vars[j]]) + float(rows[row+k+i][indice_vars[j]])    
                    somme[j] +=float(rows[row+k][indice_vars[j]])
                rows_lissees[row+k][indice_vars[j]]=somme[j]/2/k
        
        for row in range(k):
            for j in range (len(self.variables)):
                rows_lissees[row][indice_vars[j]]="NEANT"
                rows_lissees[len(rows_lissees)-row-1][indice_vars[j]]="NEANT"
        table_lissee.rows=rows_lissees
        return(table_lissee)
    
    if __name__ == '__main__':
        import doctest
        doctest.testmod()