# FONCTIONNE !
from table import Table
from operations.transformers.transformers import Transformers
from outils.indicevariable import IndiceVariable

class SelectionDonneesTransformer(Transformers):
    """Classe qui sélectionne des observations d'une table en fonction de critères de séléction sur une variable.
    
    Attributs
    ----------
    variable : str
        variable sur laquelle on va appliquer les critères de séléction
    critere_de_selection : list[str]
        criteres auxquelles les données doivent répondre par rapport à la variable définie

    Méthode
    ----------
    execute() 
        sélectionne les données qui répondent aux critères de sélection sur la variable
    
    Exemple
    ----------
    >>> data = Table(["Nom", "Age", "couleur_yeux", "date_naissance"], [["Berova", 20,"Vert", "2001-05-12"],["Foucher", 21,"Marron", "2000-03-08"],
    ["Kiren", 21, "Marron", "1999-09-06"],["Lucas",20,"Bleu", "2000-06-23"],["Rompisch",0.612,"Vert", "2020-09-20"],["Titouan",20,"Bleu", "2000-09-01"]])
    >>> new_data = SelectionDonneesTransformer("Age", [18, 19, 20, 21]).execute(data)
    >>> print(new_data.rows)
    [['Berova', 20, 'Vert', '2001-05-12'], ['Foucher', 21, 'Marron', '2000-03-08'], ['Kiren', 21, 'Marron', '1999-09-06'], ['Lucas', 20, 'Bleu', '2000-06-23'], 
    ['Titouan', 20, 'Bleu', '2000-09-01']]
    """

    def __init__(self, variable, critere_de_selection):
        """
        Attributs
        ----------
        variable : str
            variable sur laquelle on va appliquer les critères de séléction
        critere_de_selection : list[str]
            criteres auxquelles les données doivent répondre par rapport à la variable définie
        """
        super().__init__()
        self.variable = variable
        self.critere_de_selection = critere_de_selection

    def execute(self, table):
        """Sélectionne les données de la table qui répondent aux critères de sélection sur la variable

        Attribut
        ----------
        table : Table

        Retour
        -------
        Table
            table avec les données des variables qui répondent aux critères de sélection
       
        """
        header = table.header
        rows = table.rows
        indice_var = IndiceVariable(self.variable).execute(table)
        if indice_var == -1 :
            return(table)
        else:
            rows_final = []
            for row in rows:
                if row[indice_var] in self.critere_de_selection:
                    rows_final.append(row)
            return(Table(header,rows_final))

    if __name__ == '__main__':
        import doctest
        doctest.testmod()
