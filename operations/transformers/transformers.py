from abc import ABC, abstractmethod

class Transformers(ABC):
    def __init__(self) -> None:
        pass

    @abstractmethod
    def execute(self,Table):
        pass