from table import Table
from outils.indicevariable import IndiceVariable
from operations.transformers.transformers import Transformers
# FONCTIONNE !

import datetime 

class FenetrageTransformer(Transformers) :
    """Classe qui sélectionne les données dans la fenêtre de temps choisie.

    Attributs
    ----------
    d1 : date
        date de début du fenetrage
    d2 : date
        date de fin du fenetrage

    Méthode
    ----------
    execute() 
        Selectionne les données dans la fenêtre de temps
    
    Exemple
    ----------
    >>> data = Table(["Nom", "Age", "couleur_yeux", "date_naissance"], [["Berova", 20,"Vert", "2001-05-12"],["Foucher", 21,"Marron", "2000-03-08"],
    ["Kiren", 21, "Marron", "1999-09-06"],["Lucas",20,"Bleu", "2000-06-23"],["Rompisch",0.612,"Vert", "2020-09-20"],["Titouan",20,"Bleu", "2000-09-01"]])
    >>> new_data = FenetrageTransformer("2000-06-01", "2001-06-01").execute(data)
    >>> print(new_data.rows)
    [['Berova', 20, 'Vert', '2001-05-12'], ['Lucas', 20, 'Bleu', '2000-06-23'], ['Titouan', 20, 'Bleu', '2000-09-01']]
    
    """
    def __init__(self, d1, d2) :
        """Attributs
        ----------
        d1 : date
            date de début du fenetrage
        d2 : date
            date de fin du fenetrage
        """

        super().__init__()
        self.d1 = d1
        self.d2 = d2
            
    def execute(self,table) :
        """Selectionne les données dans la fenêtre de temps
        
        Attribut
        ----------
        table : Table
        
        Retour
        -------
        Table
            table avec seulement les données dans la fenêtre de temps déterminée.

        """
        header = table.header
        rows = table.rows
        rows_final = []
        indice_date=IndiceVariable("jour").execute(table)
        if indice_date != -1 and datetime.datetime.strptime(self.d1, "%Y-%m-%d")<=datetime.datetime.strptime(self.d2, "%Y-%m-%d"):
            for row in rows:
                if datetime.datetime.strptime(row[indice_date], "%Y-%m-%d") >= datetime.datetime.strptime(self.d1, "%Y-%m-%d") and datetime.datetime.strptime(row[indice_date], "%Y-%m-%d") <= datetime.datetime.strptime(self.d2, "%Y-%m-%d"):
                    rows_final.append(row)
            return(Table(header,rows_final))

    if __name__ == '__main__':
        import doctest
        doctest.testmod() 

                
    

