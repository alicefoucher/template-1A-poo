# FONCTIONNE !

from table import Table
from operations.transformers.transformers import Transformers
from outils.indicevariable import IndiceVariable

class ChangementSpatialTransformer(Transformers):
    """Classe qui ajoute à une table un nouvelle variable, région ou pays en fonction du choix de l'échelle, si elle n'existe pas déjà, afin d'associer chaque individu à une région ou à un pays.

    Attribut
    ----------
    echelle : str = "nation"
        échelle qui ne peut être que "region" ou "nation"

    Méthodes
    ----------
    execute() 
        Ajoute les variables region et pays à la table choisie
    
    deptoregion()
        Ajoute la variable region si elle n'existe pas déjà pour associer chaque individu à une région
    
    nation()
        Ajoute la variable pays si elle n'existe pas déjà pour associer chaque individu au pays "France"

    Exemple
    ----------
    >>> data = Table(["Nom", "Age", "couleur_yeux", "dep"], [["Berova", 20,"Vert", "14"],["Foucher", 21,"Marron", "14"],["Kiren", 21, "Marron", "57"],["Lucas",20,"Bleu", "14"],
    ["Rompisch",0.612,"Vert", "35"],["Titouan",20,"Bleu", "76"]])
    >>> new_data = ChangementSpatialTransformer("region").execute(data)
    >>> print(new_data.rows)
    [['Berova', 20, 'Vert', 'Normandie'], ['Foucher', 21, 'Marron', 'Normandie'], ['Kiren', 21, 'Marron', 'Grand-Est'], ['Lucas', 20, 'Bleu', 'Normandie'], 
    ['Rompisch', 0.612, 'Vert', 'Bretagne'], ['Titouan', 20, 'Bleu', 
    'Normandie']] 
    >>> print(new_data.header)
    ['Nom', 'Age', 'couleur_yeux', 'région']
    """

    def __init__(self, echelle = "nation"):
        """
        Attribut
        ----------
            echelle : str = "nation"
            échelle qui ne peut être que "region" ou "nation"
        """
        super().__init__()
        self.echelle = echelle

    def execute(self,table):
        """Ajoute les variables region et pays à la table choisie
        
        Attribut
        ----------
        table : Table
        
        Retour
        -------
        Table
            table avec la nouvelle variable selon l'échelle choisie 

        """
        if self.echelle == "region":
            return(self.deptoregion(table))
        else :
            return(self.national(table))

    def deptoregion(self,table):
        """Ajoute la variable region si elle n'existe pas déjà pour associer chaque individu à une région
        
        Attribut
        ----------
        table : Table

        Retour
        -------
        Table
            table avec la nouvelle variable région

        """
        header = table.header
        rows = table.rows
        indice_var=IndiceVariable("dep").execute(table)
        if indice_var == -1 :
            return(table)
        else:
            header[indice_var]='région'
            for ligne in rows:
                if ligne[indice_var]=="62" or ligne[indice_var]=="59" or ligne[indice_var]=="80" or ligne[indice_var]=="60" or ligne[indice_var]=="02":
                    ligne[indice_var]="Hauts-de-France"
                elif ligne[indice_var]=="08" or ligne[indice_var]=="51" or ligne[indice_var]=="10" or ligne[indice_var]=="55" or ligne[indice_var]=="52" or ligne[indice_var]=="57" or ligne[indice_var]=="54" or ligne[indice_var]=="88" or ligne[indice_var]=="67" or ligne[indice_var]=="68":
                    ligne[indice_var]="Grand-Est"
                elif ligne[indice_var]=="95" or ligne[indice_var]=="77" or ligne[indice_var]=="78" or ligne[indice_var]=="91" or ligne[indice_var]=="75" or ligne[indice_var]=="92" or ligne[indice_var]=="93" or ligne[indice_var]=="94":
                    ligne[indice_var]="Ile-de-France"
                elif ligne[indice_var]=="76" or ligne[indice_var]=="14" or ligne[indice_var]=="27" or ligne[indice_var]=="50" or ligne[indice_var]=="51":
                    ligne[indice_var]="Normandie"
                elif ligne[indice_var]=="29" or ligne[indice_var]=="22" or ligne[indice_var]=="56" or ligne[indice_var]=="35":
                    ligne[indice_var]="Bretagne"
                elif ligne[indice_var]=="53" or ligne[indice_var]=="72" or ligne[indice_var]=="44" or ligne[indice_var]=="49" or ligne[indice_var]=="85":
                    ligne[indice_var]="Pays de la Loire"
                elif ligne[indice_var]=="27" or ligne[indice_var]=="45" or ligne[indice_var]=="41" or ligne[indice_var]=="36" or ligne[indice_var]=="37" or ligne[indice_var]=="18":
                    ligne[indice_var]="Centre-Val de Loire"
                elif ligne[indice_var]=="89" or ligne[indice_var]=="58" or ligne[indice_var]=="21" or ligne[indice_var]=="39" or ligne[indice_var]=="71" or ligne[indice_var]=="25" or ligne[indice_var]=="70":
                    ligne[indice_var]="Bourgogne-Franche-Comté"
                elif ligne[indice_var]=="89" or ligne[indice_var]=="63" or ligne[indice_var]=="15" or ligne[indice_var]=="42" or ligne[indice_var]=="43" or ligne[indice_var]=="69" or ligne[indice_var]=="01" or ligne[indice_var]=="07" or ligne[indice_var]=="38" or ligne[indice_var]=="26" or ligne[indice_var]=="73" or ligne[indice_var]=="74":
                    ligne[indice_var]="Auvergne-Rhône-Alpes"
                elif ligne[indice_var]=="04" or ligne[indice_var]=="05" or ligne[indice_var]=="06" or ligne[indice_var]=="83" or ligne[indice_var]=="84" or ligne[indice_var]=="13":
                    ligne[indice_var]="Provence-Alpes-Côte d'Azur"       
                elif ligne[indice_var]=="46" or ligne[indice_var]=="12" or ligne[indice_var]=="48" or ligne[indice_var]=="30" or ligne[indice_var]=="34" or ligne[indice_var]=="81" or ligne[indice_var]=="82" or ligne[indice_var]=="31" or ligne[indice_var]=="32" or ligne[indice_var]=="65" or ligne[indice_var]=="66" or ligne[indice_var]=="09" or ligne[indice_var]=="11":
                    ligne[indice_var]="Occitanie"
                elif ligne[indice_var]=="2A" or ligne[indice_var]=="2B":
                    ligne[indice_var]="Corse"
                elif ligne[indice_var]=="971":
                    ligne[indice_var]="Guadeloupe"
                elif ligne[indice_var]=="972":
                    ligne[indice_var]="Martinique"
                elif ligne[indice_var]=="973":
                    ligne[indice_var]="Guyane"
                elif ligne[indice_var]=="974":
                    ligne[indice_var]="La Réunion"
                elif ligne[indice_var]=="976":
                    ligne[indice_var]="Mayotte"
                else:
                    ligne[indice_var]="Nouvelle-Aquitaine"
            return(Table(header,rows))
                
                
    def national(self,table):
        """Ajoute la variable pays si elle n'existe pas déjà pour associer chaque individu au pays "France"
        
        Attribut
        ----------
        table : Table

        Retour
        -------
        Table
            table avec la nouvelle variable pays

        """
        header = table.header
        rows = table.rows
        indice_var1=IndiceVariable("dep").execute(table)
        indice_var2=IndiceVariable("nomReg").execute(table)
        indice_var = max(indice_var1,indice_var2)
        header[indice_var]="pays"
        if indice_var == -1 :
            return("table inchangée")
        else:
            for ligne in rows:
                ligne[indice_var]="France"
            return(Table(header,rows))
                    
    if __name__ == '__main__':
        import doctest
        doctest.testmod() 