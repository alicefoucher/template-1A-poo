import copy
from operations.transformers.transformers import Transformers
from outils.indicevariable import IndiceVariable

class JointureTransformer(Transformers) :
    """Classe qui joint deux tables par rapport à une variable en commun. Si la jointure n'est pas possible, elle n'aura pas lieu.

    Attribut
    ----------
    table1 : Table
        Première table sur laquelle la jointure va être faite

    Méthode
    ----------
    execute() 
        Joint les deux tables données
    
    Exemple
    ----------
    >>> data1 = Table(["Nom", "Age", "couleur_yeux"], [["Berova", 20, "Vert"],["Foucher", 21,"Marron"],["Kiren", 21, "Marron"],["Lucas",20,"Bleu"],["Rompisch",0.612,"Vert"],["Titouan",20,"Bleu"]])
    >>> data2 = Table(["Nom", "dep", "date_naissance"], [["Berova", "14", "2001-05-12"],["Foucher", "14", "2000-03-08"],["Kiren", "57", "1999-09-06"],["Lucas","14", "2000-06-23"],["Rompisch","14", "2020-09-20"],["Titouan", "76", "2000-09-01"]])
    >>> new_data = JointureTransformer(data1).execute(data2)
    >>> print(new_data.header)
    ['Nom', 'Age', 'couleur_yeux', 'dep', 'date_naissance']
    >>> print(new_data.rows)
    [['Berova', 20, 'Vert', '14', '2001-05-12'], ['Foucher', 21, 'Marron', '14', '2000-03-08'], ['Kiren', 21, 'Marron', '57', '1999-09-06'], ['Lucas', 20, 'Bleu', '14', '2000-06-23'], ['Rompisch', 0.612, 'Vert', '14', '2020-09-20'], ['Titouan', 20, 'Bleu', '76', '2000-09-01']]
    """
    
    def __init__(self, table1):
        """    
        Attribut
        ----------
        table1 : Table
            Première table sur laquelle la jointure va être faite
        """
        super().__init__()
        self.table1 = table1
        self.header1 = table1.header
        self.rows1 = table1.rows

    def execute(self,table2) :
        """Joint les deux tables données

        Attribut
        ----------
        table2 : Table
            Deuxième table à joindre à la premiere

        Retour
        -------
        Table
            nouvelle table avec les deux tables jointes sur la même variable
 
        """
        table_initiale=copy.copy(self.table1)
        jointure_possible = False
        for head1 in self.header1:
            indice_var2=IndiceVariable(head1).execute(table2)
            if indice_var2 != -1 :
                jointure_possible= True
        header2 = table2.header
        rows2 = table2.rows
        
        
        if jointure_possible:
            for head in header2:
                if IndiceVariable(head).execute(self.table1)==len(self.table1.header)-1:
                    column=[]
                    indice_head=IndiceVariable(head).execute(table2)
                    for row in rows2:
                        column.append(row[indice_head])
                    self.table1.add_column(head, column, position=len(self.table1.header))
            
        else :
            return("Ces deux tables ne peuvent pas être jointes")
        
        if self.header1==table_initiale :
            return("Vous n'avez pas le bon nombre d'observations ou le bon ordre") 
        return(self.table1)

    if __name__ == '__main__':
        import doctest
        doctest.testmod() 