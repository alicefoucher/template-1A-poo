from cartes.cartoplot import CartoPlot
from outils.indicevariable import IndiceVariable
from table import Table

class MAPexporter():
    def __init__(self, var, nom="carte"):
        super().__init__()
        self.var = var
        self.nom = nom

    def executeregion(self, table):
        indice_var=IndiceVariable(self.var).execute(table)
        indice_reg=IndiceVariable("region").execute(table)
        cp = CartoPlot()
        d = {}
        rows=table.rows
        for row in rows:
            d[row[indice_reg]]=int(float(row[indice_var]))
        fig=cp.plot_dep_map(data=d, x_lim=(-10,10), y_lim=(41,52))
        fig.savefig(self.nom +'.png')

    def executedep(self, table):
        indice_var=IndiceVariable(self.var).execute(table)
        indice_dep=IndiceVariable("dep").execute(table)
        cp = CartoPlot()
        d = {}
        rows=table.rows
        for row in rows:
            d[row[indice_dep]]=int(float(row[indice_var]))
        fig=cp.plot_dep_map(data=d, x_lim=(-10,10), y_lim=(41,52))
        fig.savefig( self.nom + '.png')