from abc import ABC, abstractmethod

class Exporters(ABC):
    def __init__(self) -> None:
        pass

    @abstractmethod
    def execute(self):
        pass