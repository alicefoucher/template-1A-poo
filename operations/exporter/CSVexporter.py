from table import Table
import csv
from operations.exporter.exporters import Exporters
class CSVexporter(Exporters):
    """Classe qui exporte une table en format CSV.

    Attribut
    ----------
    nom : str
        nom de la table

    Méthode
    ----------
    execute() 
        Transforme la table en format CSV


    """
    def __init__(self, nom):
        """    
        Paramètre
        ----------
        nom : str
            nom de la table
        """
        super().__init__()
        self.nom = nom

    def execute(self, table):
        """    
        Paramètre
        ----------
        table : Table
            la table qu'on veut exporter

        Retour
        -------
        .csv
            fichier CSV des données de la table 


        """
        rows = table.rows
        with open(self.nom + '.csv','w',newline='') as fichiercsv:
            writer=csv.writer(fichiercsv)
            writer.writerow(table.header)
            for row in rows:
                writer.writerow(row)