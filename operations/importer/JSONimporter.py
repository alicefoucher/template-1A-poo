from table import Table
from operations.importer.importers import Importers

class JSONimporter(Importers):
    """Classe qui importe un fichier JSON en format Table.

    Attributs
    ----------
    calendrier_ou_academie : str
        a quel dictionnaire nous nous interessons 
    filename : str ='VacancesScolaires.json'
        Nom du fichier JSON 
    folder : str ='./data/'
        fichier

    Méthode
    ----------
    execute() 
        Transforme le fichier JSON en table

    """

    def __init__(self, calendrier_ou_academie, filename='VacancesScolaires.json', folder ='./data/' )  :
        """
        Attributs
        ----------
        calendrier_ou_academie : str
        
        filename : str ='VacancesScolaires.json'
            Nom du fichier JSON
        folder : str ='./data/'
        
        """
        self.folder = folder
        self.filename = filename
        self.calendrier_ou_academie = calendrier_ou_academie

    def execute(self):
        """Transforme un fichier JSON en table

        Retour
        -------
        Table
            table composée des données du fichier JSON 

        
        """
        import json
        with open(self.folder + self.filename) as json_file :
            data = json.load(json_file)
            
        head=list(data[str(self.calendrier_ou_academie)][1].keys())
        rows=[]
        for i in range(len(data[self.calendrier_ou_academie])):
            rows.append(list(data[str(self.calendrier_ou_academie)][i].values()))
        return(Table(head,rows))