from table import Table
from operations.importer.importers import Importers

class CSVimporter(Importers):
    """Classe qui importe un fichier CSV en format Table.

    Attributs
    ----------
    withheader : bool
        True si le fichier CSV a un header, False sinon
    filename : str
        Nom du fichier CSV
    folder : str ='./data/'
        

    Méthode
    ----------
    execute() 
        Transforme le fichier CSV en table

    """
    def __init__(self,withheader,filename,folder ='./data/'):
        """
        Attributs
        ----------
        withheader : bool
            True si le fichier CSV a un header, False sinon
        filename : str
            Nom du fichier CSV
        folder : str ='./data/'
            
        """
        self.withheader = withheader
        self.filename = filename
        self.folder = folder

    def execute(self):
        """Transforme un fichier CSV en table

        Retour
        -------
        Table
            table composée des données du fichier CSV 

        
        """
        import csv
        data = []
        with open(self.folder + self.filename, encoding='ISO-8859-1') as csvfile :
            covidreader = csv.reader(csvfile, delimiter=';')
            for row in covidreader :
                data.append(row)
            if self.withheader:
                t=Table(header=data[0], rows=data[1:])
            else:
                head=[]
                for i in range (len(data[0])):
                    head.append("col"+ str(i))
                t=Table(header=head,rows=data)
        return(t)


