# FONCTIONNE !

from table import Table
from operations.estimators.estimators import Estimators
from outils.indicevariable import IndiceVariable
from operations.estimators.moyenne import MoyenneEstimator

class Variance_ecart_typeEstimator(Estimators) :
    """Classe qui calcule la variance et l'écart-type d'une variable choisie dans une table définie. Si la variable n'est pas quantitative, la classe renvoie un message d'erreur.

    Attribut
    ----------
    variable : str
        la variable sur laquelle on fait la moyenne

    Méthodes
    ----------
    execute() 
        Renvoie la variance et l'écart-type de la variable dans une table choisie

    variance()
        Calcule la variance de la variable dans la table
    
    ecart_type()
        Calcule l'écart-type de la variable dans la table

    Exemple 
    ----------
    >>> data = Table(["Nom", "Age", "couleur_yeux"], [["Berova", 20,"Vert"],["Foucher", 21,"Marron"],["Kiren", 21, "Marron"],["Lucas",20,"Bleu"],
    ["Rompisch",0.612,"Vert"],["Titouan",20,"Bleu"]])
    >>> var_ecart_type = Variance_ecart_typeEstimator("Age").execute(data)
    >>> print(var_ecart_type)
    (54.58402, 7.388099891040998)
    """

    def __init__(self, variable) :
        """    
        Paramètre
        ----------
        variable : str
            la variable sur laquelle on va faire la variance et l'écart-type
        """
        super().__init__()
        self.variable = variable
        

    def execute(self,table):
        """ Renvoie la vairance et l'écart-type d'une variable dans une table donnée.

        Paramètre
        ----------
        table : Table

        Retour
        -------
        tuple
            tuple composé de la variance et de l'écart-type de la variable (si elle est quantitative) 
            ou de deux messages d'erreur

        """
        return(self.variance(table), self.ecart_type(table))

    def variance(self,table):
        """ Calcule la vairance d'une variable dans une table donnée.

        Paramètre
        ----------
        table : Table

        Retours
        -------
        float
            variance de la variable (si elle est quantitative) 
        str 
            message d'erreur si la variable n'est pas quantitative 

    
        """
        header = table.header
        rows = table.rows
        indvar = IndiceVariable(self.variable)
        indice_var = indvar.execute(table)
        if indice_var == -1:
            return("Variable non quantitative")
        else:
        #calcul de la variance suivant la formule classique de la variance
            S=0
            nb_var=0
            moy=MoyenneEstimator(self.variable).execute(table)
            for row in rows:
                S+= (float(row[indice_var])-moy)**2
                nb_var+=1
            return(S/nb_var)

    def ecart_type(self, table) :
        """ Calcule l'écart-type d'une variable dans une table donnée.

        Paramètre
        ----------
        table : Table

        Retours
        -------
        float
            écart-type de la variables (si elle est quantitative) 
        str 
            message d'erreur si la variable n'est pas quantitative 

        """
        header = table.header
        rows = table.rows
        indvar = IndiceVariable(self.variable)
        indice_var = indvar.execute(table)
        if indice_var == -1:
            return("Variable non quantitative")
        else:
            variance = self.variance(table)
            return(variance**(1/2))

    if __name__ == '__main__':
        import doctest
        doctest.testmod()  