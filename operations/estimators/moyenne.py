# FONCTIONNE !
from table import Table
from operations.estimators.estimators import Estimators
from outils.indicevariable import IndiceVariable


class MoyenneEstimator(Estimators):
    """Classe qui calcule la moyenne d'une variable choisie dans une table définie. Si la variable n'est pas quantitative, la classe renvoie un message d'erreur.

    Attribut
    ----------
    variable : str
        la variable sur laquelle on fait la moyenne

    Méthode
    ----------
    execute() 
        On fait en sorte d'appliquer l'algorithme de calcul de moyenne sur la table choisie par rapport à la variable

    Exemple 
    ----------
    >>> data = Table(["Nom", "Age", "couleur_yeux"], [["Berova", 20,"Vert"],["Foucher", 21,"Marron"],["Kiren", 21, "Marron"],["Lucas",20,"Bleu"],
    ["Rompisch",0.612,"Vert"],["Titouan",20,"Bleu"]])
    >>> moyenne = MoyenneEstimator("Age").execute(data)
    >>> print(moyenne)
    17.102
    """

    def __init__(self, variable):
        """    
        Paramètre
        ----------
        variable : str
            la variable sur laquelle on fait la moyenne
        """
        super().__init__()
        self.variable = variable
        
    def execute(self,table):
        """    
        Paramètre
        ----------
        table : Table
            la table sur laquelle on calcule la moyenne selon la variable choisie

        Retours
        -------
        float
            moyenne de la variable (si elle est quantitative)
        str 
            message d'erreur si la variable n'est pas quantitative 

        """
        header = table.header
        rows = table.rows
        indice_var=IndiceVariable.execute(self, table)
        if indice_var == -1:
            return("Variable non quantitative")
        else:
            nb_var = 0
            sum = 0
            for row in rows:
                nb_var += 1
                sum+=float(row[indice_var])
            return(sum/nb_var)

    if __name__ == '__main__':
        import doctest
        doctest.testmod()         
