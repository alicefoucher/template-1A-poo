# FONCTIONNE !
from table import Table
from operations.estimators.moyenne import MoyenneEstimator
from operations.estimators.estimators import Estimators
from random import randint
from outils.indicevariable import IndiceVariable
from outils.distance import Distance
import copy

class KmeansEstimator(Estimators):

    """Classe qui consiste à regrouper les éléments d'une table en groupes dont le nombre est choisi, appelés clusters (numérotés). Elle renvoie une table qui a une nouvelle colonne par rapport à 
l'ancienne dans laquelle chaque individu est associé à un cluster.

    Attributs
    ----------
    nb_classes : int
        nombre de clusters souhaités
    variables : list[str]
        liste des variables à prendre en compte dans le clustering
    compteur_max : int
        évite un temps de calcul trop long, coupe l'algorithme une fois atteint

    Méthode
    ----------
    execute() 
        Applique l'algorithme des kmeans sur la table donnée pour associer chaque individu à un cluster 
        puis créé la nouvelle table avec la variable cluster

    Exemple 
    ----------
    >>> data = Table(["Nom", "Age", "couleur_yeux","poids"], [["Berova", 20,"Vert","12"],["Foucher", 21,"Marron","70"],["Kiren", 21, "Marron","2351"],
    ["Lucas",20,"Bleu","65"],["Rompisch",0.612,"Vert","3"],["Titouan",20,"Bleu","70"]])
    >>> datacluster = KmeansEstimator(3, ["Age","poids"], 30).execute(data)
    >>> print(datacluster.header)
    ['Nom', 'Age', 'couleur_yeux', 'poids', 'classe']
    >>> print(datacluster.rows)
    [['Berova', 20, 'Vert', '12', 0], ['Foucher', 21, 'Marron', '70', 1], ['Kiren', 21, 'Marron', '2351', 2], ['Lucas', 20, 'Bleu', '65', 1], ['Rompisch', 0.612, 'Vert', '3', 0], ['Titouan', 20, 'Bleu', '70', 1]]
    """

    def __init__(self,nb_classes,variables,compteur_max):
        """
        Paramètres
        ----------
        nb_classes : int
            nombre de clusters souhaités
        variables : list[str]
            les variables à prendre en compte dans le clustering
        compteur_max : int
            évite un temps de calcul trop long
        """
        super().__init__()
        self.nb_classes=nb_classes  
        self.variables=variables
        self.compteur_max=compteur_max
        
    def execute(self,table):
        """Sert à appliquer l'algorithme des Kmeans.

        Paramètres
        ----------
        table : Table
            table sur laquelle l'algorithme sera appliqué

        Retour
        -------
        Table
            table avec la nouvelle variable classe qui associe chaque individu à son cluster 
        """

        header = table.header
        rows = table.rows
        compteur=0
        Partition=[[] for i in range(self.nb_classes)]
        Moyenne_Partition=[[] for i in range(self.nb_classes)]
        Ancienne_Partition=[[] for i in range(self.nb_classes)]
        indice_vars=[IndiceVariable(variable).execute(table) for variable in self.variables]
        
        if len(rows)<=10:
            for ind_var in range (len(indice_vars)):
                for i in range (self.nb_classes): #initialisation 
                    row=rows[i]
                    Partition[i].append(row)  
                    Moyenne_Partition[i]=[row[ind_var] for ind_var in indice_vars]
            
        else :
            for ind_var in range (len(indice_vars)):
                for i in range (self.nb_classes): #initialisation 
                    row=rows[randint(0,len(rows)-1)]
                    Partition[i].append(row)  
                    Moyenne_Partition[i]=[row[ind_var] for ind_var in indice_vars]
        
        Distance_a_la_moyenne=[[] for i in range(len(rows))]
        while Partition != Ancienne_Partition and compteur<self.compteur_max:
            compteur+=1
            Ancienne_Partition=copy.copy(Partition)
            if compteur !=1:
                Partition=[[] for i in range(self.nb_classes)]
            for j in range(len(rows)):
                bool_appartient_deja_a_une_classe=False
                row_simplifiee=[rows[j][ind_var] for ind_var in indice_vars]
                Distance_a_la_moyenne[j]=[Distance(row_simplifiee,Moyenne_Partition[i],range(len(indice_vars))).execute() for i in range (self.nb_classes) ]
                moyenne_plus_proche = min(Distance_a_la_moyenne[j])
                for i in range(self.nb_classes):
                    if not bool_appartient_deja_a_une_classe and Distance_a_la_moyenne[j][i]==moyenne_plus_proche:
                        Partition[i].append(rows[j])
                        bool_appartient_deja_a_une_classe=True #on ne peut pas appartenir à plusieurs classes en même temps
            Moyenne_Partition=[float(MoyenneEstimator(self.variables[k]).execute(Table(header, Partition[i]))) for k in range(len(self.variables)) for i in range(self.nb_classes)]
        
        #attribution finale des partitions
        column_classes=[0 for i in range(len(rows))]
        for j in range(len(rows)) :
            bool_appartient_deja_a_une_classe=False
            for i in range(self.nb_classes):
                if not bool_appartient_deja_a_une_classe :
                    if rows[j] in Partition[i]:
                        column_classes[j]=i
                        bool_appartient_deja_a_une_classe = True
        table_avec_classes=table.add_column("classe", column_classes, position=len(header))
        return(table_avec_classes)

    if __name__ == '__main__':
        import doctest
        doctest.testmod()    

        