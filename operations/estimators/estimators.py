from abc import ABC, abstractmethod

class Estimators(ABC):
    def __init__(self) -> None:
        pass

    @abstractmethod
    def execute(self):
        pass