from operations.transformers.selectionvariable import SelectionVariableTransformer
from operations.importer.JSONimporter import JSONimporter
from operations.transformers.fenetrage import FenetrageTransformer
from operations.transformers.selectiondonnees import SelectionDonneesTransformer
from operations.transformers.moyenne_glissante import MoyenneGlissanteTansformer
from operations.importer.CSVimporter import CSVimporter
import copy
from operations.transformers.moyenne_glissante import MoyenneGlissanteTansformer
from operations.estimators.kmeans import KmeansEstimator
from sorties.vacances import vacances
from pipeline import Pipeline

csv_nouveaux_covid=CSVimporter(True, folder='./data/', filename='donnees-hospitalieres-nouveaux-covid19-2021-05-11-19h05.csv')
table_nouveaux_covid= csv_nouveaux_covid.execute()

json_calendrier=JSONimporter(calendrier_ou_academie='Calendrier')
table_calendrier=json_calendrier.execute()
json_academie=JSONimporter(calendrier_ou_academie='Academie')
table_academie=json_academie.execute()

# Quel est le résultat de k-means avec k=3,
# sur les données des départements du mois de Janvier 2021,
# lissées avec une moyenne glissante de 7 jours ?
table6 = copy.copy(table_nouveaux_covid)
pile6 = Pipeline()
pile6.add_step(MoyenneGlissanteTansformer(["hosp","rea","rad","dc"], 7))
pile6.add_step(KmeansEstimator(variables=["hosp","rea","rad","dc"], nb_classes=3, compteur_max=100))
pile6.add_step(SelectionVariableTransformer(["dep","incid_hosp","incid_rea","incid_dc","classe","incid_rad"]))
pile6.add_step(FenetrageTransformer("2021-01-01","2021-01-31"))
pile6.add_step(SelectionDonneesTransformer("sexe",["0"]))
resultat6=pile6.run(table6)
print(resultat6.header)
print(resultat6.rows[1:20])