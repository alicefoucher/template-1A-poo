from operations.importer.CSVimporter import CSVimporter
import copy
from sorties.nbentre2datespardep import NbEntreDeuxDatesParDep
from pipeline import Pipeline


csv_nouveaux_covid=CSVimporter(True, folder='./data/', filename='donnees-hospitalieres-nouveaux-covid19-2021-05-11-19h05.csv')
table_nouveaux_covid= csv_nouveaux_covid.execute()

# Combien de nouvelles hospitalisations ont eu lieu ces 7 derniers jours dans chaque département?
table2 = copy.copy(table_nouveaux_covid)
pile2 = Pipeline()
pile2.add_step(NbEntreDeuxDatesParDep("incid_hosp","2021-05-04","2021-05-11"))
print((pile2.run(table2)).rows)
#a= NbEntreDeuxDatesParDep("incid_hosp","2021-05-04","2021-05-11").execute(table_nouveaux_covid)
# print(a.rows)