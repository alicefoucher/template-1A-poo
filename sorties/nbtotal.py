# FONCTIONNE !
# ATTENTION AUX VARIABLES CUMULEES

from table import Table
from outils.indicevariable import IndiceVariable
from operations.transformers.transformers import Transformers
from operations.estimators.estimators import Estimators
from sorties.sorties import Sorties

class NbTotal(Sorties): 
    """Classe qui calcule le nombre total de la variable donnée dans une table.
    
    Attribut
    ----------
    variable : str
        variable sur laquelle on va calculer le total

    Méthode
    ----------
    execute() 
        calcule le total de la variable donnée dans la table
    
    Exemple
    --------
    >>> table = Table(["jour", "hosp", "rea", "dc"], [["2021-01-01",3,1,0], ["2021-01-02",5,4,3], ["2021-01-03",0,0,0], ["2021-01-04",7,2,1],["2021-01-05",5,2,0]])
    >>> NT = NbTotal("hosp").execute(table)
    >>> print(NT)
    20.0 
    """
    def __init__(self, variable):
        """
        Attribut
        ----------
        variable : str
            variable sur laquelle on va calculer le total

        """
        super().__init__()
        self.variable = variable

    def execute(self,table):
        """Calcule le total de la variable donnée dans la table

        Attribut
        ----------
        table : Table

        Retours
        -------
        float
            total de la variabe dans la table
        str 
            message d'erreur si c'est impossible
        
        """
        header = table.header
        rows = table.rows
        indice_var=IndiceVariable(self.variable).execute(table)
        if indice_var == -1 :
            return("il n'y a pas la variable attendue dans cette table")
        else:
            sum = 0
            for row in rows:
                sum += float(row[indice_var])
            return(sum)
                

