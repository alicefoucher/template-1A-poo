# FONCTIONNE
# table : donnees-hospitalieres-nouveaux-covid19-2021-03-03-17h03.csv
from table import Table
from outils.indicevariable import IndiceVariable
from operations.transformers.transformers import Transformers
from operations.estimators.estimators import Estimators
from sorties.sorties import Sorties
from sorties.nbtotal import NbTotal
from operations.transformers.fenetrage import FenetrageTransformer
from operations.transformers.selectiondonnees import SelectionDonneesTransformer

class NbEntreDeuxDatesParDep(Sorties): 
    """Classe qui  calcule, par département, le nombre de la variable donnée dans l'intervalle de temps choisi. 
    Le résultat est fournis sous la forme d'une 
    
    Attributs
    ----------
    variable : str
        variable sur laquelle on va calculer le total
    date_deb : date
        date du début 
    date_fin : date
        date de la fin

    Méthode
    ----------
    execute() 
        calcule le total de la variable donnée dans la table sur l'intervalle de temps
    
    Exemple
    --------
    >>> table = Table(["dep", "jour", "hosp", "rea", "dc"], [["01", "2021-01-01",3,1,0],["02", "2021-01-02",3,1,0],["03", "2021-01-02",3,1,0],["04", "2021-01-03",5,1,0], ["01", "2021-01-03",3,6,0],["02", "2021-01-01",7,1,0],["01", "2021-01-04",3,3,0],["02", "2021-01-01",9,1,0],["03", "2021-01-02",0,1,0],["02", "2021-01-01",7,1,0],])
    >>> NT = NbEntreDeuxDatesParDep("hosp", "2021-01-02", "2021-01-04").execute(table)
    >>> print(NT.rows[:4])
    [['01', '6.0'], ['02', '3.0'], ['03', '3.0'], ['04', '5.0']]
    """

    def __init__(self, variable, date_deb, date_fin):
        """
        Attributs
        ----------
        variable : str
            variable sur laquelle on va calculer le total
        date_deb : date
            date du début 
        date_fin : date
            date de la fin
        """
        super().__init__()
        self.variable = variable
        self.date_fin = date_fin
        self.date_deb = date_deb

    def execute(self,table):
        """Calcule le nombre total de la variable entre les deux dates dans la table

        Attribut
        ----------
        table : Table

        Retours
        -------
        Table
            table comprenant une colonne pour les départements et une colonne pour le nombre total entre les deux dates calculé
        str
            message d'erreur si l'opération est impossible

        """
        header = table.header
        rows = table.rows
        indice_var=IndiceVariable(self.variable).execute(table)
        indice_dep=IndiceVariable("dep").execute(table)
        if indice_var == -1 :
            return("opération impossible")
        else:
            tab=FenetrageTransformer(self.date_deb,self.date_fin).execute(table)
            tab.rows.sort(key=lambda x: x[indice_dep])
            new_rows=[]
            for dep in range(1,96):
                if dep<10:
                    tab_dep=SelectionDonneesTransformer("dep",["0"+str(dep)]).execute(tab)
                    new_rows.append(["0"+str(dep),str(NbTotal(self.variable).execute(tab_dep))])
                else:
                    tab_dep=SelectionDonneesTransformer("dep",[str(dep)]).execute(tab)
                    new_rows.append([str(dep),str(NbTotal(self.variable).execute(tab_dep))])
            for dep in range(971,977):
                tab_dep=SelectionDonneesTransformer("dep",[str(dep)]).execute(tab)
                new_rows.append([str(dep),NbTotal(self.variable).execute(tab_dep)])
            new_rows.append(["2A",str(NbTotal(self.variable).execute(SelectionDonneesTransformer("dep",["2A"]).execute(tab)))])
            new_rows.append(["2B",str(NbTotal(self.variable).execute(SelectionDonneesTransformer("dep",["2B"]).execute(tab)))])
            return(Table(["dep","nb_"+ self.variable],new_rows))