from operations.estimators.moyenne import MoyenneEstimator
from operations.transformers.fenetrage import FenetrageTransformer
from table import Table
from datetime import datetime, timedelta

class EvolutionMoyenneParSemaine2:
    """Classe qui renvoie liste contenant les intervalles de temps, c'est-à-dire les deux semaines consécutives dans la table sur lesquelles on cherche à savoir 
    le taux d'évolution d'une variable donnée
    
    Attributs
    ----------
    date_deb : date
        ???
    date_max : date 
        ???
    variable : str
        variable dans la table sur laquelle on voudra calculer le taux d'évolution

    Méthode
    ----------
    execute() 
        trouve la liste d'intervalles de temps des deux semaines consécutives dans la table
    
    """

    def __init__(self, date_deb, date_max, variable):
        """
        Attributs
        ----------
        date_deb : date
            ???
        date_max : date 
            ???
        variable : str
            variable dans la table sur laquelle on voudra calculer le taux d'évolution

        """
        self.date_deb = date_deb
        self.date_max = date_max
        self.variable = variable

    def execute(self,table):
        """Trouve la liste d'intervalles de temps des deux semaines consécutives dans la table

        Attribut
        ----------
        table : Table

        Retour
        -------
        list
            liste des intervalles de temps des deux semaines consécutives présentes dans la table
        
        """
        header = table.header
        max = datetime.strptime(self.date_max, "%Y-%m-%d")
        rows_final = []
        header = ["dates"] + [self.variable]
        date_deb1= self.date_deb
        date_fin1 = str(datetime.strptime(self.date_deb, "%Y-%m-%d")+ timedelta(7))[:10]
        date_deb2 = str(datetime.strptime(self.date_deb, "%Y-%m-%d") + timedelta(8))[:10]
        date_fin2 = str(datetime.strptime(self.date_deb, "%Y-%m-%d") + timedelta(15))[:10]
        table1= FenetrageTransformer(date_deb1, date_fin1).execute(table)
        moy1 = MoyenneEstimator(self.variable).execute(table1)
        while datetime.strptime(date_fin2, "%Y-%m-%d") <= max:
            row=[str(date_fin2[5:10])]
            #row=[str(date_deb1) + " / " + str(date_fin1) + " & " + str(date_deb2) + " / " + str(date_fin2)]
            table2= FenetrageTransformer(date_deb2, date_fin2).execute(table)
            moy2 = MoyenneEstimator(self.variable).execute(table2)
            evol=((moy2-moy1)/moy1)*100
            date_deb1 = date_deb2
            date_fin1 = date_fin2
            date_deb2 = str(datetime.strptime(date_fin1, "%Y-%m-%d") + timedelta(1))[:10]
            date_fin2 = str(datetime.strptime(date_fin1, "%Y-%m-%d") + timedelta(8))[:10] 
            row.append(evol)
            rows_final.append(row)
            moy1 = moy2
        return(Table(header, rows_final))

    if __name__ == '__main__':
        import doctest
        doctest.testmod()

