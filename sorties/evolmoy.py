# FONCTIONNE 
# table : donnees-hospitalieres-nouveaux-covid19-2021-03-03-17h03.csv
from table import Table
from outils.indicevariable import IndiceVariable
from operations.transformers.transformers import Transformers
from operations.estimators.estimators import Estimators
from sorties.sorties import Sorties
from sorties.nbentre2datespardep import NbEntreDeuxDatesParDep
from operations.estimators.moyenne import MoyenneEstimator
from sorties.nbtotal import NbTotal
from datetime import datetime

class EvolutionMoyenne(Sorties): 
    """Classe qui calcule le taux d'évolution d'une variable dans une table sur deux périodes de temps données.
    
    Attributs
    ----------
    variable : str
        variable sur laquelle on cherche le taux d'évolution
    date_deb1 : date
        début de la première période de temps
    date_fin1 : date
        fin de la première période de temps
    date_deb2 : date
        début de la deuxième période de temps
    date_fin2 : date
        fin de la deuxième période de temps

    Méthode
    ----------
    execute() 
        calcule le taux d'évolution de la variable sur les deux périodes de temps
    
    """

    def __init__(self, variable, date_deb1, date_fin1, date_deb2, date_fin2):
        """
        Attributs
        ----------
        variable : str
            variable sur laquelle on cherche le taux d'évolution
        date_deb1 : date
            début de la première période de temps
        date_fin1 : date
            fin de la première période de temps
        date_deb2 : date
            début de la deuxième période de temps
        date_fin2 : date
            fin de la deuxième période de temps
        """
        self.variable = variable
        self.date_deb1 = date_deb1
        self.date_fin1 = date_fin1
        self.date_deb2 = date_deb2
        self.date_fin2 = date_fin2
        
    def execute(self,table):
        """Calcule le taux d'évolution de la variable dans la table sur les deux périodes de temps dans la table

        Attribut
        ----------
        table : Table

        Retours
        -------
        float
            taux d'évolution de la variable sur deux périodes de temps
        str
            message d'erreur si l'indice de la variable est -1
        
        """
        header = table.header
        rows = table.rows
        indice_var=IndiceVariable(self.variable).execute(table)
        indice_dep=IndiceVariable("dep").execute(table)
        if indice_var == -1 :
            return("opération impossible")
        else:
            tab1 = NbEntreDeuxDatesParDep(self.variable,self.date_deb1,self.date_fin1).execute(table)
            tab2 = NbEntreDeuxDatesParDep(self.variable,self.date_deb2,self.date_fin2).execute(table)
            nb_jour1 = abs((datetime.strptime(self.date_fin1, "%Y-%m-%d") - datetime.strptime(self.date_deb1, "%Y-%m-%d")).days)
            nb_jour2 = abs((datetime.strptime(self.date_fin2, "%Y-%m-%d") - datetime.strptime(self.date_deb2, "%Y-%m-%d")).days)
            moy1 = NbTotal(self.variable).execute(tab1)/nb_jour1
            moy2 = NbTotal(self.variable).execute(tab2)/nb_jour2
            return(((moy2-moy1)/moy1)*100)
        
    if __name__ == '__main__':
        import doctest
        doctest.testmod()