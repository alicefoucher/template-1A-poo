from operations.transformers.selectionvariable import SelectionVariableTransformer

from operations.importer.JSONimporter import JSONimporter
from table import Table
from outils.indicevariable import IndiceVariable
from operations.transformers.fenetrage import FenetrageTransformer
from operations.transformers.selectiondonnees import SelectionDonneesTransformer
from sorties.sorties import Sorties
from sorties.nbtotal import NbTotal
import time
from datetime import datetime
from datetime import timedelta
from operations.importer.CSVimporter import CSVimporter


def vacances(quel_total_calculer, quelles_vacances, annee_scolaire,table_nouveaux_covid,table_calendrier,table_academie):
    '''
    permet de calculer le nombre de "quel_total_calculer" (par exemple, le nombre de réanimations) la semaine suivant des
    vacances (en l'occurence ici de la Toussaint de 2020).
    '''
        
# comment connaître les dates des vacances de la Toussaint en 2020 selon les zones ? 
    table_calendrier=SelectionVariableTransformer(['Description','Zone','annee_scolaire','Debut','Fin']).execute(table_calendrier)
    data_zone_date =[]
    for i in range(len(table_calendrier.rows)):
        row=table_calendrier.rows[i]
        indice_description=IndiceVariable('Description').execute(table_calendrier)
        indice_annee_scolaire=IndiceVariable('annee_scolaire').execute(table_calendrier)
        if row[indice_description]==str(quelles_vacances) :
            if row[indice_annee_scolaire]==str(annee_scolaire) :
               data_zone_date.append(row)
    table_zone_date=Table(table_calendrier.header, data_zone_date)
    table_zone_date=SelectionVariableTransformer(['Zone','Debut','Fin']).execute(table_zone_date)

    indice_date_debut=IndiceVariable('Debut').execute(table_zone_date)
    indice_date_fin=IndiceVariable('Fin').execute(table_zone_date)

    #on s'interesse à la semaine après les vacances de la Toussaint  
    for row in table_zone_date.rows:
        row[indice_date_debut]=str(datetime.strptime(str(row[indice_date_debut]), "%Y-%m-%d")+ timedelta(7))[:10]
        row[indice_date_fin]=str(datetime.strptime(str(row[indice_date_fin]), "%Y-%m-%d")+ timedelta(7))[:10]

    rows=[]
    for i in range(len(table_zone_date.rows)):
        if table_zone_date.rows[i] not in table_zone_date.rows[0:i] :
            rows.append(table_zone_date.rows[i])
    table_zone_date.rows=rows
    # on remarque que l'on n'a pas de données pour tous les DOM TOM
    table_academie=SelectionDonneesTransformer("Zone", ["Zone A", "Zone B", "Zone C", "Guyane","Mayotte"]).execute(table_academie)
    table_academie=SelectionVariableTransformer(['dep','Zone']).execute(table_academie)

    # nous allons diviser le calcul selon les zones :
    liste_A, liste_B, liste_C, liste_Mayotte, liste_Guyane =[], [], [], [], []
    
    for i in range(len(table_academie.rows)):
        zone=str(table_academie.rows[i][1])
        row=table_academie.rows[i]
        if zone=="Zone A":
            liste_A.append(str(row[0]))
        elif zone=="Zone B":
            liste_B.append(str(row[0]))
        elif zone=="Zone C":
            liste_C.append(str(row[0]))
        elif zone=="Guyane":
            liste_Guyane.append(str(row[0]))
        elif zone=="Mayotte":
            liste_Mayotte.append(str(row[0]))  

    table_zone_A=SelectionDonneesTransformer("dep",liste_A).execute(table_nouveaux_covid)
    table_zone_B=SelectionDonneesTransformer("dep",liste_B).execute(table_nouveaux_covid)
    table_zone_C=SelectionDonneesTransformer("dep",liste_C).execute(table_nouveaux_covid)
    table_Mayotte=SelectionDonneesTransformer("dep",liste_Mayotte).execute(table_nouveaux_covid)
    table_Guyane=SelectionDonneesTransformer("dep",liste_Guyane).execute(table_nouveaux_covid)
     
    indice_date_debut=IndiceVariable('Debut').execute(table_zone_date)
    indice_date_fin=IndiceVariable('Fin').execute(table_zone_date)

    date_debut=table_zone_date.rows[2][indice_date_debut]
    date_fin=table_zone_date.rows[2][indice_date_fin]
    table_zone_A=FenetrageTransformer(date_debut,date_fin).execute(table_zone_A)
    
    date_debut=table_zone_date.rows[0][indice_date_debut]
    date_fin=table_zone_date.rows[0][indice_date_fin]
    table_zone_B=FenetrageTransformer(date_debut,date_fin).execute(table_zone_B)
    
    date_debut=table_zone_date.rows[3][indice_date_debut]
    date_fin=table_zone_date.rows[3][indice_date_fin]
    table_zone_C=FenetrageTransformer(date_debut,date_fin).execute(table_zone_C)
    
    date_debut=table_zone_date.rows[4][indice_date_debut]
    date_fin=table_zone_date.rows[4][indice_date_fin]
    table_Guyane=FenetrageTransformer(date_debut,date_fin).execute(table_Guyane)
    
    date_debut=table_zone_date.rows[1][indice_date_debut]
    date_fin=table_zone_date.rows[1][indice_date_fin]
    table_Mayotte=FenetrageTransformer(date_debut,date_fin).execute(table_Mayotte)

    nb_total_zone_A = NbTotal(str(quel_total_calculer)).execute(table_zone_A)
    nb_total_zone_B = NbTotal(str(quel_total_calculer)).execute(table_zone_B)
    nb_total_zone_C = NbTotal(str(quel_total_calculer)).execute(table_zone_C)
    nb_total_Mayotte = NbTotal(str(quel_total_calculer)).execute(table_Mayotte)
    nb_total_Guyane = NbTotal(str(quel_total_calculer)).execute(table_Guyane)
    nb_total_rea=nb_total_Guyane+nb_total_Mayotte+nb_total_zone_A+nb_total_zone_B+nb_total_zone_C
    return(nb_total_rea)