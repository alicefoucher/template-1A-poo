from abc import ABC, abstractmethod

class Sorties(ABC):
    def __init__(self) -> None:
        pass

    @abstractmethod
    def execute(self,table):
        pass