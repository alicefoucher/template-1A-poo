
from operations.transformers.selectiondonnees import SelectionDonneesTransformer
from operations.importer.CSVimporter import CSVimporter
from sorties.nbtotal import NbTotal
import copy
from pipeline import Pipeline

csv_classe_age=CSVimporter(True, folder='./data/', filename='donnees-hospitalieres-classe-age-covid19-2021-05-11-19h05.csv')
table_classes_age= csv_classe_age.execute()


#quelles categories d'age sont les plus impactées ?
tranche_age = ["09", "19" ,"29", "39", "49", "59", "69", "79", "89", "90"]
hosp_age =[]
rea_age =[]
dc_age =[]
for age in tranche_age :
        
    table0 = copy.copy(table_classes_age)
    pile0 = Pipeline()
    pile0.add_step(NbTotal("hosp"))
    pile0.add_step(SelectionDonneesTransformer("cl_age90",[age]))

    pile01= Pipeline()
    pile01.add_step(NbTotal("rea"))
    pile01.add_step(SelectionDonneesTransformer("cl_age90",[age]))

    pile02 = Pipeline()
    pile02.add_step(NbTotal("dc"))
    pile02.add_step(SelectionDonneesTransformer("cl_age90",[age]))
    pile02.add_step(SelectionDonneesTransformer("jour", ["2021-03-03"]))

    hosp_age.append(pile0.run(table0))
    rea_age.append(pile01.run(table0))
    dc_age.append(pile02.run(table0))
    
max_hosp =[0,0]
for ind, elt in enumerate(hosp_age) :
    if elt == max([elt for elt in hosp_age]) :
        max_hosp = [ind, elt]


max_rea =[0,0]
for ind, elt in enumerate(rea_age) :
    if elt == max([elt for elt in rea_age]) :
        max_rea = [ind, elt]


max_dc =[0,0]
for ind, elt in enumerate (dc_age) :
    if elt == max([elt for elt in dc_age]) :
        max_dc = [ind, elt]
    

print(max_hosp, max_rea, max_dc)