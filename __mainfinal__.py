from operations.transformers.selectionvariable import SelectionVariableTransformer
from operations.importer.JSONimporter import JSONimporter
from table import Table
from operations.estimators.estimators import Estimators
from operations.transformers.transformers import Transformers
from operations.transformers.changementspatial import ChangementSpatialTransformer
from outils.indicevariable import IndiceVariable
from operations.transformers.fenetrage import FenetrageTransformer
from operations.transformers.jointure import JointureTransformer
from operations.transformers.normalisation import NormalisationTransformer
from operations.transformers.selectiondonnees import SelectionDonneesTransformer
from operations.transformers.moyenne_glissante import MoyenneGlissanteTansformer
from operations.estimators.moyenne import MoyenneEstimator
from operations.estimators.variance_ecart_type import Variance_ecart_typeEstimator
from operations.importer.CSVimporter import CSVimporter
from sorties.sorties import Sorties
from sorties.nbtotal import NbTotal
import time 
import copy
from datetime import datetime
from datetime import timedelta
from operations.transformers.moyenne_glissante import MoyenneGlissanteTansformer
from operations.estimators.kmeans import KmeansEstimator
from sorties.evolmoyparsem import EvolutionMoyenneParSemaine2
from sorties.nbentre2datespardep import NbEntreDeuxDatesParDep
from sorties.evolmoy import EvolutionMoyenne
from sorties.vacances import vacances
from pipeline import Pipeline


if __name__ == "__main__":

    csv_cumulees = CSVimporter(True, folder='./data/', filename='donnees-hospitalieres-covid19-2021-05-11-19h05.csv')
    table_cumulees = csv_cumulees.execute()

    csv_nouveaux_covid=CSVimporter(True, folder='./data/', filename='donnees-hospitalieres-nouveaux-covid19-2021-05-11-19h05.csv')
    table_nouveaux_covid= csv_nouveaux_covid.execute()

    csv_classe_age=CSVimporter(True, folder='./data/', filename='donnees-hospitalieres-classe-age-covid19-2021-05-11-19h05.csv')
    table_classes_age= csv_classe_age.execute()

    json_calendrier=JSONimporter(calendrier_ou_academie='Calendrier')
    table_calendrier=json_calendrier.execute()
    json_academie=JSONimporter(calendrier_ou_academie='Academie')
    table_academie=json_academie.execute()
    
# Quel est le nombre total d’hospitalisations dues au Covid-19?
    #table1 = copy.copy(table_cumulees)
    #pipe1 = Pipeline()
    #pipe1.add_step(NbTotal("dc"))
    #pipe1.add_step(SelectionDonneesTransformer("sexe",["0"]))
    #pipe1.add_step(FenetrageTransformer("2021-03-03","2021-03-03"))
    #print(pipe1.run(table1))
    #tab=FenetrageTransformer("2021-03-03","2021-03-03").execute(table_cumulees)
    #tab=SelectionDonneesTransformer("sexe",["0"]).execute(tab)
    #print(NbTotal("dc").execute(tab))


# Combien de nouvelles hospitalisations ont eu lieu ces 7 derniers jours dans chaque département?
    #table2 = copy.copy(table_nouveaux_covid)
    #pile2 = Pipeline()
    #pile2.add_step(NbEntreDeuxDatesParDep("incid_hosp","2021-02-25","2021-03-03"))
    #print((pile2.run(table2)).rows)
    #a= NbEntreDeuxDatesParDep("incid_hosp","2021-02-25","2021-03-03").execute(table_nouveaux_covid)
    #print(a.rows)

# Comment évolue la moyenne des nouvelles hospitalisations journalières de cette semaine par rapport à celle de la semaine dernière ?
    #table3 = copy.copy(table_nouveaux_covid)
    #pile3 = Pipeline()
    #pile3.add_step(EvolutionMoyenne("incid_hosp","2021-02-10","2021-02-17","2021-02-18","2021-02-25"))
    #resultat3=pile3.run(table3)
    
# Evolution moyenne par semaine:
    #table4 = copy.copy(table_cumulees)
    #pile4 = Pipeline()
    #pile4.add_step(EvolutionMoyenneParSemaine2("2021-02-03", "2021-03-03", ["incid_hosp"]))
    #resultat4=pile4.run(table4)
    #print(resultat4.rows)
    #print(str((datetime.strftime(datetime.strptime("2021-03-03", "%Y-%m-%d") + timedelta(15),"%Y-%m-%d"))))
    #a=EvolutionMoyenneParSemaine2("2021-02-03", "2021-03-03", ["incid_hosp"]).execute(table_cumulees)
    #print(a.rows)
    #print(str((datetime.strftime(datetime.strptime("2021-03-03", "%Y-%m-%d") + timedelta(15),"%Y-%m-%d"))))

# Quel est le nombre total d’hospitalisations dues au Covid-19 ?
    #table5 = copy.copy(table_cumulees)
    #pile5 = Pipeline()
    #pile5.add_step(NbTotal("dc"))
    #pile5.add_step(SelectionDonneesTransformer("sexe",["0"]))
    #pile5.add_step(FenetrageTransformer("2021-03-03","2021-03-03"))
    #resultat =pile5.run(table5)
    #tab=FenetrageTransformer("2021-03-03","2021-03-03").execute(table_nouveaux_covid)
    #print(tab.rows)
    #tab=SelectionDonneesTransformer("sexe",["0"]).execute(tab)
    #print(tab.rows)
    #print(NbTotal("dc").execute(tab))

# Quel est le résultat de k-means avec k=3,
    # sur les données des départements du mois de Janvier 2021,
    # lissées avec une moyenne glissante de 7 jours ?
    # table6 = copy.copy(table_nouveaux_covid)
    # pile6 = Pipeline()
    # pile6.add_step(MoyenneGlissanteTansformer(["hosp","rea","rad","dc"], 7)).add_step(KmeansEstimator(variables=["hosp","rea","rad","dc"], nb_classes=3, compteur_max=100)).add_step(SelectionVariableTransformer(["dep","incid_hosp","incid_rea","incid_dc","classe","incid_rad"])).add_step(FenetrageTransformer("2021-01-01","2021-01-31")).add_step(SelectionDonneesTransformer("sexe",["0"]))
    # resultat6=pile6.run(table6)
    # print(resultat6.rows)
    # print(resultat6.header)
       

# Combien de nouvelles admissions en réanimation ont eu lieu pendant la semaine suivant les vacances de la Toussaint de 2020 ?
    #print(vacances("incid_rea","Toussaint","2020-2021"))

# Les femmes sont-elles plus impactées par le covid-19 que les hommes ?
    # "hosp";"rea";"rad";"dc"
    #table7 = copy.copy(table_cumulees)

    # Pour les femmes : 
    #pile7 = Pipeline()
    #pile7.add_step(NbTotal('hosp')).add_step(SelectionDonneesTransformer("sexe",["2"])).add_step(SelectionDonneesTransformer("jour",["2021-03-03"]))
    #pile71= Pipeline()
    #pile71.add_step(NbTotal("rea")).add_step(SelectionDonneesTransformer("sexe",["2"])).add_step(SelectionDonneesTransformer("jour",["2021-03-03"]))
    #pile72 = Pipeline()
    #pile72.add_step(NbTotal("dc")).add_step(SelectionDonneesTransformer("sexe",["2"])).add_step(SelectionDonneesTransformer("jour",["2021-03-03"]))
    #femmes = [pile7.run(table7), pile71.run(table7), pile72.run(table7)]
    
    #Pour les hommes : 
    #table8 = copy.copy(table_cumulees)
    #pile8 = Pipeline()
    #pile8.add_step(NbTotal('hosp')).add_step(SelectionDonneesTransformer("sexe",["1"])).add_step(SelectionDonneesTransformer("jour",["2021-03-03"]))
    #pile81= Pipeline()
    #pile81.add_step(NbTotal("rea")).add_step(SelectionDonneesTransformer("sexe",["1"])).add_step(SelectionDonneesTransformer("jour",["2021-03-03"]))
    #pile82 = Pipeline()
    #pile82.add_step(NbTotal("dc")).add_step(SelectionDonneesTransformer("sexe",["1"])).add_step(SelectionDonneesTransformer("jour",["2021-03-03"]))
    #hommes = [pile8.run(table8), pile81.run(table8), pile82.run(table8)]

    #print(str(hommes[0]-femmes[0]) +" hommes de plus que les femmes ont été hospitalisées,", str(hommes[1]-femmes[1]) + " hommes de plus que de femmes ont été en réanimation et", str(hommes[2]-femmes[2]) + " hommes de plus que de femmes sont décédées de la covid-19.")


    #table_femmes=SelectionDonneesTransformer("sexe",["2"]).execute(SelectionDonneesTransformer("jour",["2021-03-03"]).execute(table_cumulees))
    #table_hommes=SelectionDonneesTransformer("sexe",["1"]).execute(SelectionDonneesTransformer("jour",["2021-03-03"]).execute(table_cumulees))
    #femmes=[NbTotal('hosp').execute(table_femmes),NbTotal("rea").execute(table_femmes), NbTotal("dc").execute(table_femmes)]
    #hommes=[NbTotal('hosp').execute(table_hommes),NbTotal("rea").execute(table_hommes), NbTotal("dc").execute(table_hommes)]
    #print(str(hommes[0]-femmes[0]) +" hommes de plus que les femmes ont été hospitalisées", str(hommes[1]-femmes[1]) + "hommes de plus que de femmes ont été en réanimation", str(hommes[2]-femmes[2]) + "hommes de plus que de femmes sont décédées de la covid-19.")
   
#quelles categories d'age sont les plus impactées ?
    #tranche_age = ["09", "19" ,"29", "39", "49", "59", "69", "79", "89", "90"]
    #hosp_age =[]
    #rea_age =[]
    #dc_age =[]
    #for age in tranche_age :
        
    #    table0 = copy.copy(table_classes_age)
    #    pile0 = Pipeline().add_step(NbTotal("hosp")).add_step(SelectionDonneesTransformer("cl_age90",[age]))
    #    pile01= Pipeline().add_step(NbTotal("rea")).add_step(SelectionDonneesTransformer("cl_age90",[age]))
    #    pile02 = Pipeline().add_step(NbTotal("dc")).add_step(SelectionDonneesTransformer("cl_age90",[age])).add_step(SelectionDonneesTransformer("jour", ["2021-03-03"]))

    #    hosp_age.append(pile0.run(table0))
     #   rea_age.append(pile01.run(table0))
     #   dc_age.append(pile02.run(table0))
    
    #max_hosp =[0,0]
    #for ind, elt in enumerate(hosp_age) :
        #if elt == max([elt for elt in hosp_age]) :
            #max_hosp = [ind, elt]


    #max_rea =[0,0]
    #for ind, elt in enumerate(rea_age) :
        #if elt == max([elt for elt in rea_age]) :
           # max_rea = [ind, elt]


    #max_dc =[0,0]
    #for ind, elt in enumerate (dc_age) :
       #if elt == max([elt for elt in dc_age]) :
          # max_dc = [ind, elt]
    

#print(max_hosp, max_rea, max_dc)