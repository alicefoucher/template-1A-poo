
from operations.transformers.selectiondonnees import SelectionDonneesTransformer
from sorties.nbtotal import NbTotal
import copy
from pipeline import Pipeline
from operations.importer.CSVimporter import CSVimporter

# Les femmes sont-elles plus impactées par le covid-19 que les hommes ?
# "hosp";"rea";"rad";"dc"
csv_cumulees = CSVimporter(True, folder='./data/', filename='donnees-hospitalieres-covid19-2021-05-11-19h05.csv')
table_cumulees = csv_cumulees.execute()

table7 = copy.copy(table_cumulees)

# Pour les femmes : 
pile7 = Pipeline()
pile7.add_step(NbTotal('hosp'))
pile7.add_step(SelectionDonneesTransformer("sexe",["2"]))
pile7.add_step(SelectionDonneesTransformer("jour",["2021-03-03"]))
pile71= Pipeline()
pile71.add_step(NbTotal("rea"))
pile71.add_step(SelectionDonneesTransformer("sexe",["2"]))
pile71.add_step(SelectionDonneesTransformer("jour",["2021-03-03"]))
pile72 = Pipeline()
pile72.add_step(NbTotal("dc"))
pile72.add_step(SelectionDonneesTransformer("sexe",["2"]))
pile72.add_step(SelectionDonneesTransformer("jour",["2021-03-03"]))
femmes = [pile7.run(table7), pile71.run(table7), pile72.run(table7)]
    
#Pour les hommes : 
table8 = copy.copy(table_cumulees)
pile8 = Pipeline()
pile8.add_step(NbTotal('hosp'))
pile8.add_step(SelectionDonneesTransformer("sexe",["1"]))
pile8.add_step(SelectionDonneesTransformer("jour",["2021-03-03"]))
pile81= Pipeline()
pile81.add_step(NbTotal("rea"))
pile81.add_step(SelectionDonneesTransformer("sexe",["1"]))
pile81.add_step(SelectionDonneesTransformer("jour",["2021-03-03"]))
pile82 = Pipeline()
pile82.add_step(NbTotal("dc"))
pile82.add_step(SelectionDonneesTransformer("sexe",["1"]))
pile82.add_step(SelectionDonneesTransformer("jour",["2021-03-03"]))
hommes = [pile8.run(table8), pile81.run(table8), pile82.run(table8)]

print(str(hommes[0]-femmes[0]) +" hommes de plus que les femmes ont été hospitalisés,", str(hommes[1]-femmes[1]) + " hommes de plus que de femmes ont été en réanimation et", str(hommes[2]-femmes[2]) + " hommes de plus que de femmes sont décédés de la covid-19.")


    #table_femmes=SelectionDonneesTransformer("sexe",["2"]).execute(SelectionDonneesTransformer("jour",["2021-03-03"]).execute(table_cumulees))
    #table_hommes=SelectionDonneesTransformer("sexe",["1"]).execute(SelectionDonneesTransformer("jour",["2021-03-03"]).execute(table_cumulees))
    #femmes=[NbTotal('hosp').execute(table_femmes),NbTotal("rea").execute(table_femmes), NbTotal("dc").execute(table_femmes)]
    #hommes=[NbTotal('hosp').execute(table_hommes),NbTotal("rea").execute(table_hommes), NbTotal("dc").execute(table_hommes)]
    #print(str(hommes[0]-femmes[0]) +" hommes de plus que les femmes ont été hospitalisées", str(hommes[1]-femmes[1]) + "hommes de plus que de femmes ont été en réanimation", str(hommes[2]-femmes[2]) + "hommes de plus que de femmes sont décédées de la covid-19.")
   