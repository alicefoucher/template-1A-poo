
from operations.importer.CSVimporter import CSVimporter
from sorties.nbentre2datespardep import NbEntreDeuxDatesParDep
from operations.exporter.MAPexporter import MAPexporter

csv_nouveaux_covid=CSVimporter(True, folder='./data/', filename='donnees-hospitalieres-nouveaux-covid19-2021-05-11-19h05.csv')
table_nouveaux_covid= csv_nouveaux_covid.execute()
 
tabledep= NbEntreDeuxDatesParDep("incid_hosp","2021-02-03","2021-02-03").execute(table_nouveaux_covid)
MAPexporter("nb_incid_hosp","TEST2").executedep(tabledep)