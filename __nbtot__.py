from operations.transformers.selectionvariable import SelectionVariableTransformer
from operations.importer.JSONimporter import JSONimporter
from table import Table
from operations.estimators.estimators import Estimators
from operations.transformers.transformers import Transformers
from operations.transformers.changementspatial import ChangementSpatialTransformer
from outils.indicevariable import IndiceVariable
from operations.transformers.fenetrage import FenetrageTransformer
from operations.transformers.jointure import JointureTransformer
from operations.transformers.normalisation import NormalisationTransformer
from operations.transformers.selectiondonnees import SelectionDonneesTransformer
from operations.transformers.moyenne_glissante import MoyenneGlissanteTansformer
from operations.estimators.moyenne import MoyenneEstimator
from operations.estimators.variance_ecart_type import Variance_ecart_typeEstimator
from operations.importer.CSVimporter import CSVimporter
from sorties.sorties import Sorties
from sorties.nbtotal import NbTotal
import time 
import copy
from datetime import datetime
from datetime import timedelta
from operations.transformers.moyenne_glissante import MoyenneGlissanteTansformer
from operations.estimators.kmeans import KmeansEstimator
from sorties.evolmoyparsem import EvolutionMoyenneParSemaine2
from sorties.nbentre2datespardep import NbEntreDeuxDatesParDep
from sorties.evolmoy import EvolutionMoyenne
from sorties.vacances import vacances
from pipeline import Pipeline


csv_cumulees = CSVimporter(True, folder='./data/', filename='donnees-hospitalieres-covid19-2021-05-11-19h05.csv')
table_cumulees = csv_cumulees.execute()
    
# Quel est le nombre total d’hospitalisations dues au Covid-19?
table1 = copy.copy(table_cumulees)
pipe1 = Pipeline()
pipe1.add_step(NbTotal("dc"))
pipe1.add_step(SelectionDonneesTransformer("sexe",["0"]))
pipe1.add_step(FenetrageTransformer("2021-05-11","2021-05-11"))
print(pipe1.run(table1))
    #tab=FenetrageTransformer("2021-05-11","2021-05-11").execute(table_cumulees)
    #tab=SelectionDonneesTransformer("sexe",["0"]).execute(tab)
    #print(NbTotal("dc").execute(tab))