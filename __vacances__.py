from operations.transformers.selectionvariable import SelectionVariableTransformer
from operations.importer.JSONimporter import JSONimporter
from table import Table
from operations.estimators.estimators import Estimators
from operations.transformers.transformers import Transformers
from operations.transformers.changementspatial import ChangementSpatialTransformer
from outils.indicevariable import IndiceVariable
from operations.transformers.fenetrage import FenetrageTransformer
from operations.transformers.jointure import JointureTransformer
from operations.transformers.normalisation import NormalisationTransformer
from operations.transformers.selectiondonnees import SelectionDonneesTransformer
from operations.transformers.moyenne_glissante import MoyenneGlissanteTansformer
from operations.estimators.moyenne import MoyenneEstimator
from operations.estimators.variance_ecart_type import Variance_ecart_typeEstimator
from operations.importer.CSVimporter import CSVimporter
from sorties.sorties import Sorties
from sorties.nbtotal import NbTotal
import time 
import copy
from datetime import datetime
from datetime import timedelta
from operations.transformers.moyenne_glissante import MoyenneGlissanteTansformer
from operations.estimators.kmeans import KmeansEstimator
from sorties.evolmoyparsem import EvolutionMoyenneParSemaine2
from sorties.nbentre2datespardep import NbEntreDeuxDatesParDep
from sorties.evolmoy import EvolutionMoyenne
from sorties.vacances import vacances
from pipeline import Pipeline

csv_nouveaux_covid=CSVimporter(True, folder='./data/', filename='donnees-hospitalieres-nouveaux-covid19-2021-05-11-19h05.csv')
table_nouveaux_covid= csv_nouveaux_covid.execute()

json_calendrier=JSONimporter(calendrier_ou_academie='Calendrier')
table_calendrier=json_calendrier.execute()

json_academie=JSONimporter(calendrier_ou_academie='Academie')
table_academie=json_academie.execute()

# Combien de nouvelles admissions en réanimation ont eu lieu pendant la semaine suivant les vacances de la Toussaint de 2020 ?
print(vacances("incid_dc","Toussaint","2020-2021",table_nouveaux_covid,table_calendrier,table_academie))