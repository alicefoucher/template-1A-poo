from table import Table
from outils.indicevariable import IndiceVariable
import matplotlib.pyplot as plt

class Graphique():
    """Classe qui renvoie un graphique avec un titre qui a pour abscisse la première variable et pour ordonnée la deuxième variable d'une même table.
    
    Attributs
    ----------
    var1 : str
        variable dont les données de la table seront en abscisse
    var2 : str
        variable dont les données de la table seront en ordonnée
    titre : str
        titre du graphique

    Méthode
    ----------
    execute() 
        affiche et trace le graphe des deux variables de la table
    
    """
    def __init__(self,var1,var2,titre):
        """
        Attributs
        ----------
        var1 : str
            variable dont les données de la table seront en abscisse
        var2 : str
            variable dont les données de la table seront en ordonnée
        titre : str
            titre du graphique
        """
        self.var1= var1
        self.var2=var2
        self.titre=titre

    def execute(self,table):
        """Affiche et trace le graphe des deux variables de la table

        Attribut
        ----------
        table : Table

        Retour
        -------
        matplotlib.plot
            graphique des deux variables du tableau avec le titre 
        
        """
        plt.title(self.titre)
        rows = table.rows
        ind_var1 = IndiceVariable(self.var1).execute(table)
        ind_var2 = IndiceVariable(self.var2).execute(table)
        x=[]
        y=[]
        for row in rows:
            x.append(row[ind_var1])
            y.append(row[ind_var2])
        plt.plot(x, y)
        plt.xlabel(self.var1)
        plt.ylabel(self.var2)
        plt.show()