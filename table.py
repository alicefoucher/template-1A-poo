#FONCTIONNE !
import copy

class Table() :
    """Classe qui défini le format Table et qui peut y apporter des modifications.
    
    Attributs
    ----------
    header : list[str]
        l'ensemble des variables
    rows : list[list]
        ensemble des observations

    Méthodes
    ----------
    add_row()
        Ajoute une observation à la table
    
    remove_row()
        Retire une observation à la table
    
    add_column()
        Ajoute une variable et le données relatives à cette variables à chaque observation
    
    remove_column()
        Supprime une variable et le données relatives à cette variables à chaque observation
    
    
    """
    def __init__(self, header, rows) :
        """
        Attributs
        ----------
        header : list[str]
            l'ensemble des variables
        rows : list[list]
            ensemble des observations
        """
        self.header = header
        self.rows = rows
    
    def add_row(self, row, position = -1) :
        """Ajoute une observation à la table

        Attributs
        ----------
        row : list
        position : int = -1

        Retour
        -------
        Table
            table avec la nouvelle ligne
        
        """
        new_rows = copy.copy(self.rows)
        new_rows.append(row)
        new_rows[position] = row
        for i in range(position, len(new_rows)-1) :
            old_row=self.rows[i]
            new_rows[i + 1] = old_row
        self.rows = new_rows
    
    def remove_row(self, position) :
        """Retire une observation à la table

        Attribut
        ----------
        position = int

        Retour
        -------
        Table
            table sans la ligne 
        
        """
        del(self.rows[position])
    
    def add_column(self, name, column, position) :       
        """Ajoute une variable et le données relatives à cette variables à chaque observation

        Attributs
        ----------
        name : str
            le nom de la variable à ajouter
        column : list
            la colonne à ajouter dans la table
        position : int
            la position de la variable à laquelle on voudrait que la variable soit
        
        Retours
        -------
        Table
            table avec la nouvelle colonne 
        str
            la position n'est pas valide

        """
        if float(position)>=0:
            ''' On ajoute le nom de la colonne '''
            new_col_names = copy.copy(self.header)
            new_col_names.append(name)
            new_col_names[position] = name
            for i in range(position, len(new_col_names)-1) :
                old_col_name=self.header[i]
                new_col_names[i + 1] = old_col_name
            self.header = new_col_names
           
            ''' On ajoute les éléments de la colonne dans les lignes'''
            new_rows = copy.copy(self.rows)
            for i in range (len(column)) :
                new_rows[i].insert(position,column[i])
            self.rows = new_rows
            return(self)

        else : 
            return ("veuillez entrer une position positive" )

        
    
    def remove_column(self, position = -1) :
        """Supprime une variable et le données relatives à cette variables à chaque observation

        Attribut
        ----------
        position : int = -1

        Retour
        -------
        Table
            table sans la colonne 
        
        """

        ''' On enlève le nom de la colonne '''
        del(self.header[position])
        
        
        ''' On enlève les données de la colonne dans chaque ligne '''
        for i in range (len(self.rows)) :
            del(self.rows[i][position])
