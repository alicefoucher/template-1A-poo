from table import Table
from operations.estimators.estimators import Estimators
from operations.transformers.transformers import Transformers
from operations.transformers.changementspatial import ChangementSpatialTransformer
from outils.indicevariable import IndiceVariable
from operations.transformers.fenetrage import FenetrageTransformer
from operations.transformers.jointure import JointureTransformer
from operations.transformers.normalisation import NormalisationTransformer
from operations.transformers.selectiondonnees import SelectionDonneesTransformer
from operations.transformers.selectionvariable import SelectionVariableTransformer
from operations.transformers.moyenne_glissante import MoyenneGlissanteTansformer
from operations.estimators.moyenne import MoyenneEstimator
from operations.estimators.variance_ecart_type import Variance_ecart_typeEstimator
from operations.importer.CSVimporter import CSVimporter
from sorties.evolmoy import EvolutionMoyenne
from sorties.sorties import Sorties
from sorties.nbtotal import NbTotal
from sorties.nbentre2datespardep import NbEntreDeuxDatesParDep
from outils.distance import Distance
import time
import json
from operations.estimators.kmeans import KmeansEstimator
from operations.importer.JSONimporter import JSONimporter


csv_cumulees = CSVimporter(True, folder='./data/', filename='donnees-hospitalieres-covid19-2021-03-03-17h03.csv')
table_cumulees = csv_cumulees.execute()

csv_nouveaux_covid=CSVimporter(True, folder='./data/', filename='donnees-hospitalieres-nouveaux-covid19-2021-03-03-17h03.csv')
table_nouveaux_covid= csv_nouveaux_covid.execute()
    
json_calendrier=JSONimporter(calendrier_ou_academie='Calendrier')
table_calendrier=json_calendrier.execute()
json_academie=JSONimporter(calendrier_ou_academie='Academie')
table_academie=json_academie.execute()

if __name__ == "__main__":
    #table_nouveaux_covid=IndiceVariable("dc")
    #print(a.execute(table))

    #transfo=ChangementSpatialTransformer("region")
    #b=transfo.execute(table)

    #fen=FenetrageTransformer("2020-03-18", "2020-03-19")
    #c=fen.execute(table)

    # d=MoyenneEstimator("rea")
    # print(d.execute(csv.execute()))

    # Moygli=MoyenneGlissanteTansformator("dc",3).execute(table).rows[0:10]

    # e=Variance_ecart_typeEstimator("rea").execute(table)

    # f= NormalisationTransformer("rea").execute(table).rows[1:5]

    #g=SelectionDonneesTransformer("jour",["2020-09-28"])
    #tab=g.execute(table)

    # t=Table(["Nom", "Age", "couleur_yeux"], [["Berova", 20,"Vert"],["Foucher", 21,"Marron"],["Kiren", 21, "Marron"],["Lucas",20,"Bleu"],["Rompisch",0.612,"Vert"],["Titouan",20,"Bleu"]])
    # t.add_row(["Jean-Charles",36,"Jaune"],position=3)
    # t.remove_row(3)
    # t.add_column("poids", [20,42,35,28,69,56],position=3)
    # t.remove_column(3)

    # table3=JointureTransformer(table).execute(table2)

    # k=KmeansEstimator(3, variable="rea", compteur_max=10)
    # print(k.execute(table))
    
    #g=SelectionDonnesTransformer("jour",["2020-03-18","2020-03-19"]).execute(table)
    #taable=KmeansEstimator(5, ["rea","dc"], 60).execute(g)
    #print(taable.rows[:10])

    #json_calendrier=JSONimporter(calendrier_ou_academie='Calendrier')
    #print(json_calendrier.execute().rows)
    
    #join=JointureTransformer(table_calendrier).execute(table_academie)

    #data = Table(["Nom", "Age", "couleur_yeux", "date_naissance"], [["Berova", 20,"Vert", "2001-05-12"],["Foucher", 21,"Marron", "2000-03-08"],["Kiren", 21, "Marron", "1999-09-06"],["Lucas",20,"Bleu", "2000-06-23"],["Rompisch",0.612,"Vert", "2020-09-20"],["Titouan",20,"Bleu", "2000-09-01"]])
    #taux_ev = EvolutionMoyenne("Age","2000-01-01", "2000-06-01", "2000-06-02", "2001-01-01").execute(data)
    #print(taux_ev) 
    
    table = Table(["dep", "jour", "hosp", "rea", "dc"], [["01", "2021-01-01",3,1,0],["02", "2021-01-02",3,1,0],["03", "2021-01-02",3,1,0],["04", "2021-01-03",5,1,0], ["01", "2021-01-03",3,6,0],["02", "2021-01-01",7,1,0],["01", "2021-01-04",3,3,0],["02", "2021-01-01",9,1,0],["03", "2021-01-02",0,1,0],["02", "2021-01-01",7,1,0],])
    NT = NbEntreDeuxDatesParDep("hosp", "2021-01-02", "2021-01-04").execute(table)
    print(NT.rows[:4])