from operations.transformers.selectionvariable import SelectionVariableTransformer
from operations.importer.JSONimporter import JSONimporter
from table import Table
from operations.estimators.estimators import Estimators
from operations.transformers.transformers import Transformers
from operations.transformers.changementspatial import ChangementSpatialTransformer
from outils.indicevariable import IndiceVariable
from operations.transformers.fenetrage import FenetrageTransformer
from operations.transformers.jointure import JointureTransformer
from operations.transformers.normalisation import NormalisationTransformer
from operations.transformers.selectiondonnees import SelectionDonneesTransformer
from operations.transformers.moyenne_glissante import MoyenneGlissanteTansformer
from operations.estimators.moyenne import MoyenneEstimator
from operations.estimators.variance_ecart_type import Variance_ecart_typeEstimator
from operations.importer.CSVimporter import CSVimporter
from sorties.sorties import Sorties
from sorties.nbtotal import NbTotal
import time 
import copy
from datetime import datetime
from datetime import timedelta
from operations.transformers.moyenne_glissante import MoyenneGlissanteTansformer
from operations.estimators.kmeans import KmeansEstimator
from sorties.evolmoyparsem import EvolutionMoyenneParSemaine2
from sorties.nbentre2datespardep import NbEntreDeuxDatesParDep
from sorties.evolmoy import EvolutionMoyenne
from sorties.vacances import vacances

class Pipeline:
    
    """Classe qui créé une pile d'opérartions étape par étape puis l'applique sur une table.
    
    Attribut
    ----------
    operations : list = []
        la pile de toutes les opérations dans l'ordre qui sera appliquée sur la table


    Méthodes
    ----------
    add_step() 
        ajoute une opération à faire dans la pile
    
    execute()
        applique la pile d'opérations sur la table

    
    """
    def __init__(self):
        """
        Attribut
        ----------
        operations : list = []
            pile initialement vide
        
        """
        self.operations = []
    
    def add_step(self,step):
        """Ajoute une opération à faire dans la pile

        Attribut
        ----------
        step : Operation
            operation à ajouter dans la pile d'opérations
        """
        self.operations.append(step)

    def run(self,table) -> Table :
        """Applique la pile d'opérations sur la table  
        
        Attribut
        ----------
        table : t=Table
            la table sur laquelle on va appliquer toutes les opérations souhaitées

        Retour
        -------
        Retourne le resultat apès que toutes les opérations ont été appliquées
        
        """     
        n=len(self.operations)
        for k in range(n) :
            table = self.operations[-1].execute(table)
            self.operations.pop()
        return table