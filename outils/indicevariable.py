#FONCTIONNE !

from table import Table

class IndiceVariable():
    """Classe qui cherche l'indice de la variable dans la table.
    
    Attribut
    ----------
    variable : str
        variable dont on cherche l'indice dans la table

    Méthode
    ----------
    execute() 
        cherche l'indice de la variable dans la table
    
    Exemple
    ----------
    >>> data = Table(["Nom", "Age", "couleur_yeux", "date_naissance"], [["Berova", 20,"Vert", "2001-05-12"],["Foucher", 21,"Marron", "2000-03-08"],["Kiren", 21, "Marron", "1999-09-06"],["Lucas",20,"Bleu", "2000-06-23"],["Rompisch",0.612,"Vert", "2020-09-20"],["Titouan",20,"Bleu", "2000-09-01"]])
    >>> ind = IndiceVariable("couleur_yeux").execute(data)
    >>> print(ind)
    2

    """
    def __init__(self, variable):
        """
        Attribut
        ----------
        variable : str
            variable dont on cherche l'indice dans la table
        """
        self.variable = variable
    
    def execute(self,table):
        """Cherche l'indice de la variable dans la table

        Attribut
        ----------
        table : Table
            table dans laquelle on cherche l'indice de la variable  

        Retour
        -------
        int
            indice de la variable 

        """
        header = table.header
        indice_var = 0
        while indice_var<(len(header)-1) and header[indice_var]!=self.variable:
            indice_var+=1
        if indice_var == len(header):
            return(-1)
        else:
            return (indice_var)
    
    if __name__ == '__main__':
        import doctest
        doctest.testmod()