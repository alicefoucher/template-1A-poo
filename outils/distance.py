class Distance() :
    """Classe qui calcule la distance entre deux listes de même longueur représentant deux observations par rapport à une liste de variables quantitatives.

    Attribut
    ----------
    liste1 : list
        première observation
    liste2 : list
        deuxième observation
    ind_variables : list[int]
        liste des indices des variables quantitatives

    Méthode
    ----------
    execute() 
        Calcule la distance entre les deux observations sur les variables quantitatives 

    Exemple
    ----------
    >>> liste1 = ["Foucher", 21,"Marron", "2000-03-08"]
    >>> liste2 = ["Rompisch",0.612,"Vert", "2020-09-20"]
    >>> distance = Distance(liste1, liste2, [1]).execute()
    >>> print(distance)
    20.388
    """
    def __init__(self, liste1, liste2, ind_variables):
        """ 
        Attribut
        ----------
        liste1 : list
            première observation
        liste2 : list
            deuxième observation
        ind_variables : list[int]
            liste des indices des variables quantitatives
            
        """
        self.liste1=liste1
        self.liste2=liste2
        self.ind_variables=ind_variables

    def execute(self):
        """Calcule la distance entre les deux observations sur les variables quantitatives

        Retour
        -------
        float
            distance

        """
        somme=0
        for ind_var in self.ind_variables:
            somme+=(float(self.liste1[ind_var])-float(self.liste2[ind_var]))**2
        return (somme**0.5)
    
    if __name__ == '__main__':
        import doctest
        doctest.testmod()